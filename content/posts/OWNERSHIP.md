---
title: Ownership - an evolving and restricting term
date: 2022-01-01
author: CSDUMMI
---

I'm writing this document on a device that I own.
I own this device in the sense, that I have exclusive
control to do with this device as I see fit - as long as
I do not violate an existing law.

This is a very old but not the oldest definition of the word "own".
And the term "own" and "ownership" have undewent some quite enormous
mutations that I'll go over to some degree in this article.

The oldest definiton for ownership I could identify is that of
unconditional control over the owned thing from other humans.
If I own a rock, spear or some meat according to this definition, no other
human can affect this object except for me.

But the thing I own can still be affected by nature, climate,
heat, time and God to name but a few non-human forces acting upon
the things I own in this sense.

And this is a widely used definition of the word "own". I'm sure that it's the definition
hunters and gatheres could have used but especially early pastoral and agricultural
societies would have declared their ownership by this definition because it relies
on no other entities for it's own validity. My ownership of a flock of sheep or a
plot of land is valid as long as I can keep other humans from controlling it.

Yet it's also not a very useful definition of ownership once a society
grows somewhat more complex and authorities begin to arise that require
certain access to resources that somebody else owns.
These authorities can of course be bandits or conquerors - but also the kingdoms
that arise from these bandits and conquerors continue to access resources that
other people own without their consent in the form of taxes and confiscation,
which means once these kingdoms become more established a new definition of
ownership has to arise to allow for intervention of the state in

This is how we arrive through many iterations of kings and kingships at the
English definition of ownership. In the English system a free person that owns
anything has exclusive rights to this thing from anybody else except on the grounds
of the law, which is herein retroactively instituted as the source of all ownership
since law is the origin of all rights.
One can see this definition of ownership in pratice again, in the imposition of taxes
during the history of the English monarchy. Because once it had been established that
law on taxes could only be passed by parliament it became one of it's most significant
powers and when they used this tool against their own King, it sparked a civil war.

The exclusive right to an owned thing except on the grounds of the law is a definition of
ownership still largely followed by western and especially anglican states today in many
ordinary matters such as tools, household objects and sometimes real estate.

Though the relationship between things and their owners has become increasingly complex
and thus subject to exploitation and vulnerable to fraud in modern times.
If we look at the last example of real estate, we have to admit that few people today
actually own a house in the sense outlined above, because their rights to a house
are not actually limited only be the law but also by certain other constraints - such as a
mortage.

This means that a person with a mortage does not actually have exclusive rights to a house
but only as long as they can fulfill the conditions of this mortage and thus conditions they
negotiated often before owning a house.

And a mortage is still a very simple construction of ownership when compared to
immaterial things created for the sole purpose of being owned by people.
These immaterial things include companies, stock market shares, securities or other such ideas
of modern hypercapitalist economical absurdity.

But while they create ever more complex definition of what it means to own them, these
objects still retain a certain "control" to them. Owning a company entails control over
that companies decisions, future and profits. Owning a stock entails limited control
over company decisions, dividends. And owning a security provides the ability to sell
that security and gain some advantage depending on the type of security.

Now we get to the most recent and controversial definition of ownership pioneered and
influenced by the creation and wider adoption of Cryptocurrency and digital decentralized
consensus systems.

Because in these systems we move as far away from the first definition of ownership presented
as currently possible. A cryptocurrency is akin to a tradeable security without any additional benefits. The ownership of some cryptocurrency only provides you with the control over the next person to sell it to.

An important difference between securities and cryptocurrencies is though, that while securities
give the owner the right to trade the security, cryptocurrencies only give their owner the ability
to trade them. Because a right is based on the law and the law is created by the state - something that cryptocurrency is ignorant to - the definition of ownership of cryptocurrencies cannot be a right but must be an ability that the technology of cryptocurrency gives to the owner.

We came from a definition of ownership that was defined as control over an owned thing only
limited by nature itself to a definition of ownership that only gives the owner tradability and nothing else.

A variance of definitions of this magnitude must clearly have consequences on our modern world -
because if ownership is defined so differently by different situations and things and if ownership
today is no longer defined by the owner but by the owned thing, ownership risks becoming a meaningless concept incomprehensible to people and unusable for human interaction.
We have seen this demise of commonly used concepts already in the course of our digital
and interconnected world. Concepts like truth, control, trust and ownership have been
mutated by this new wold in a way that makes them unrecongnizable compared to their original
understanding.
