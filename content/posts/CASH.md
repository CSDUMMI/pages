---
title: Mutating Secrets digital Currency
author: CSDUMMI
date: 2022-01-01
---
## More Environmental, Accessible and Private Digital Cash

Imagine I wanted to develop a secrets based digital currency.
A naive implementation of such a currency would
represent a coin by some secret and to own a coin somebody'd have
to know that secret.

Transfering a coin would be done by sending the secret to another person through a secure channel.

But this system has an obvious problem:
the previous owner of a coin never forgets the secret and they
can always transfer the coin again to other people thus spending the coin twice.

In order to fix this problem of double spending I can create a process
that a new owner of a coin executes when they received the coin and prevents the previous owner from using their secret again:
1. they sign the coin's secret and use their signature as the coin's new secret.
2. they publish the hash of the previous secret to a public distributed database.

In other words: they claim before the world at larger that a coin has been transfered and that
the secret that was previously used to validate the coin is no longer valid. But they do so in a way that no leak of information about the coin or it's ownership history occures by only publishing the previous secret's hash.[^1]

If a node receives a secret they verify that the secret's hash is not already contained in the distributed database and if
that is so they accept the coin otherwise they reject it.

Verification of a coin solves the problem of double spending - because once a secret is contained in a database it cannot be used in a transfer again as long as the receiver does verify all incoming coins.

How do we mint new coins in this system and verify that a coin received has been issued properly?

A simple solution to this problem would be to have any coin transfer accompanied by a list
of all the previous secrets, their owners public key and their entry in the distributed database going back to the minting of the coin.
This list can then be verified by verifying the coin at each entry during the list.

How coins are minted is left to the implementation - there can be an authority setup to
mint new coins (akin to a central bank), some fixed supply be created when the network is created or some other process implemented to setup a controlled inflation. Because this system does not have miners or validators there is no place to insert newly minted coins that'd directly help network operations and if a network decides to, newly minted coins could even
be rewarded to charity.

## Analysis of this mutating secrets currency system
A mutating secrets currency system as described above has several cash-like properties:

#### distributed
A transaction can be made without any centralized or decentralized authority approving
or being involved in the transaction.

A transaction only requires the transfer of information and writing to an append-only distributed database.

#### private
Anyone not involved in a transaction can only see, that a transaction happend.
But not who or to whom something was transferred.

And only the future owners of a coin can trace the ownership history of a coin,
but even this history could obfuscated using ring signatures instead of public key signatures. [^3]

#### anonymous
An account's balance and transaction history is only available to the account themselves - if they choose to record it.
It would require an attacker to buy up every coin that a person ever owned in order reconstruct their transaction history and they'd have to do so without knowing the total number of coins an account ever owned because no record of this is kept in plain or obfuscated form on the network.

#### no consensus mechanism
Because the only distributed database is al append-only log of all the hashes of transfered secrets there is no need
for a consensus mechanism like proof of work or proof of stake for this network.
No distributed ledger of all transactions exists, that would have to be verified by a peer to peer system of validators -
the history of a coin is verified when it's transfered and the receiver of the coin has a vested interest in ensuring the validity of a coin - because an invalid coin could not be used in further transactions without high risk of being rejected.

Because there is no consenus mechanism, this system also lacks transaction fees and miners or validators and the enormous impact on the environment that these
produce.

#### reasonably secure
I do not claim that this system is satisifying the same definition of security as cryptocurrencies based on a blockchain are.

These currencies' security is defined in terms of a 51% attack:
>  The system is secure as long as honest nodes collectively control more CPU power than any cooperating group of attacker nodes. [^2]

The security definition of a mutating secrets currency is more ndoe centric than that:
*The mutating secrets currency system is reasonably secure if possesion of the current coin's secret is required to transfer them from one person to another and if a node can verify the validity of any coin they receive.*

##### Network latency attack
I call this system reasonably secure because there exists a network latency attack if the network latency is not zero.

An attacker could utilize a non-zero network latency to simultaneously transfer the same coin to two different nodes.
If these nodes immediatly claimed and verified a coin at the same time before their database version synchronized, they'd
verify the same coin provide some service or good to the attacker and only afterwards realized that one of their claims was invalidated because they entered their claim a little bit later into the database than the other node.

This kind of attack is possible although I attest significant effort on the side of the attacker to actually execute it.
And the likelyhood of such an attack can easily be reduced if the two nodes waited a randomized timeout between claiming and verifying a coin in order to synchronize databases and invalidate one of the coins.

##### accessible
I believe - though I have no proof of this nor had this paper reviewed - that a mutating secrets currency system could be a viable alternative to existing blockchain based currencies while being far more cash-like than those blockchain systems.

An example of how this system is superior to Blockchain technology is that in a mutating secrets system, the owner only needs to send a coin's current secret and it's ownership history to another person in order to transfer the coin.

This property of mutating secrets currencies makes them far more accessible to people than existing cryptocurrencies, because other than
cryptocurrencies, I can transfer mutating secrets coins through any secure communications channel - which are far more readily available
to people than cryptocurrency wallets.

##### inefficiency
There are factors in this system that can make it very inefficient:
1. Coin's ownership history is ever growing. If a coin has been transfered 100 times, it's grown in size by a factor of 100.
2. The public distributed database will grow enormously. If every node is required to keep a copy of the entire database, these nodes will require a lot of storage space.
3. While applying a transaction onto a Blockchain Currency is almost entirely independent of the amount transfered, transfering 10 coins is about 10 times faster than 100 because each individual coins has to be seperatly verified.

But given the enormous costs in power, energy, computation and money needed to
operate the current cryptocurrencies I find these potential inefficiencies both
acceptable and reducable through optimization.

In hopes of review, feedback and critique,
CSDUMMI.


[^1]: Publishing the secret itself would leak information because the previous secret would very likely also be a signature that could be brute forced to find it's public key and thus the previous owner.

[^2]: Satoshi Nakamoto, Bitcoin Whitepaper (https://bitcoin.org/bitcoin.pdf)
