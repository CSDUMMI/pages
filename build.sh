#!/usr/bin/env bash
rm -rf pages.git public/*

hugo

REPO=${1:-git@codeberg.org:CSDUMMI/website}
DATE=$(date -u -Is)
git clone $REPO pages.git

cd pages.git
git switch --orphan current
rsync -av ../public/* .
cp ../.domains .domains
git add -A
git commit -m "Deployment at $DATE"
git branch -m pages
git push -f origin pages
cd ..
rm -rf pages.git
